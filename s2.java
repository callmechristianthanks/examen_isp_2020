package com.company;

public class Main2 {

    public static void main(String[] args) {
        EThread1 t1 = new EThread1();
        EThread2 t2 = new EThread2();

        t1.start();
        t2.start();
    }

    static class EThread1 extends Thread {
        int msgnr = 0;

        public void run()
        {
            while (true)
            {
                try {
                    Thread.sleep(4000);
                    for(int i = 0; i < 10; i++) System.out.println("Ana are " + msgnr++); //thread name "Ana are"
                }catch(Exception e){}
            }
        }
    }

    static class EThread2 extends Thread{
        int msgnr = 0;

        public void run()
        {
            while (true)
            {
                try {
                    Thread.sleep(4000);
                    for(int i = 0; i < 10; i++) System.out.println("mere " + msgnr++); //thread name "mere"
                }catch(Exception e){}
            }
        }
    }


}
